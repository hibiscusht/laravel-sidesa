<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Models\Background;
use App\Http\Service;

class Profilelatar extends Controller
{
    public function upload(Request $request)
    {
         //cek apakah kategori gambar sudah ada
        $cek = Background::where("type","=",$request->type)->get()->count();
        if($cek > 0){
            return response(["status"=>"duplicate entry"],400);
        } else {
         $upload_type = $request->type;
         $file_mime_type = $request->file('file')->getClientMimeType();
         $ext = ".txt";
         switch($file_mime_type){
            case "image/png": $ext = ".png"; break;
            case "image/jpeg": $ext = ".jpeg"; break;
            case "application/pdf": $ext = ".pdf"; break;
         }
         $fname = $upload_type . '-' . rand(10000,99999) . $ext;
         $request->file('file')->move(base_path('public/files'),$fname);
         $bg = new Background();
         $bg->type = $upload_type;
         $bg->filename = $fname;
         $bg->save();
         return response(['status'=>'success','path'=>$fname],200);
        }
    }

    public function list(Request $request)
    {
        $rec_total = 0;
        $rec_filt = 0;
        $data = [];
        $term = $request->search["value"];
        // rec_total tanpa search 
        if(is_null($term)){
            $rec_total = Background::all()->count(); 
            $data = Background::skip($request->start)->take($request->length)->orderBy('id','desc')->get();
            $rec_filt = $rec_total;
        } else {
            $rec_total = Background::all()->count(); 
            $data = Background::where('filename','LIKE',"%$term%")->skip($request->start)->take($request->length)->orderBy('id','desc')->get();
            $rec_filt = Background::where('filename','LIKE',"%$term%")->get()->count(); 
        }
        $no = 1 + intval($request->start);
       
            foreach($data as $row){
                $row->no = $no;
                $no++;
            }
        
        return response([
            "draw"=>$request->draw ?? 0,
            "recordsTotal"=>$rec_total,
            "recordsFiltered"=>$rec_filt,
            "data"=>$data 
        ],200);
    }

    public function search(Request $request)
    {
        $res = Background::find($request->id);
        return response(['status'=>'success','data'=>$res],200);
    }

    public function update(Request $request)
    {
        //cek apakah kategori gambar sudah ada
        $cek = Background::where("type","=",$request->type)->where("id","!=",$request->id)->get()->count();
        if($cek > 0){
            return response(["status"=>"duplicate entry"],400);
        } else {
            $bg = Background::find($request->id);
            if($request->file("file") == "undefined" || is_null($request->file("file"))){
                $bg->type = $request->type;
                $bg->save();
            } else {
                $upload_type = $request->type;
                $file_mime_type = $request->file('file')->getClientMimeType();
                $ext = ".txt";
                switch($file_mime_type){
                   case "image/png": $ext = ".png"; break;
                   case "image/jpeg": $ext = ".jpeg"; break;
                   case "application/pdf": $ext = ".pdf"; break;
                }
                $fname = $upload_type . '-' . rand(10000,99999) . $ext;
                $request->file('file')->move(base_path('public/files'),$fname); 
                $bg->type = $upload_type;
                $bg->filename = $fname;
                $bg->save();
            }
            return response(["status"=>"success"],200);
        }
    }

    public function delete(Request $request)
    {
        $bg = Background::find($request->id);
        $bg->delete();
        return response(["status"=>"success"],200);
    }
}
