<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Models\Wrgmap;
use App\Http\Service;

class Profilemap extends Controller
{
    public function add(Request $request)
    {
         //cari tahun data
         $sch = Wrgmap::where('nik','=',$request->nik)->get()->count();
         if($sch > 0){
            return response(["status"=>"duplicate entry"],400);
         } else {
            $ed = new Wrgmap();
            $ed->nik = $request->nik;
            $ed->nama_kk = $request->nama_kk;
            $ed->jml_kelg = $request->jml_kelg;
            $ed->lat = $request->lat;
            $ed->lng = $request->lng; 
            $ed->save();
            return response(["status"=>"success"],200);
         }
    }

    public function list(Request $request)
    {
         $res = Wrgmap::all(); 
         return response(["status"=>"success","data"=>$res],200);
    }

   public function search(Request $request)
    {
        $res = Wrgmap::find($request->id);
        return response(['status'=>'success','data'=>$res],200);
    }

    public function update(Request $request)
    {
    
            $ed =  Wrgmap::find($request->id);
            $ed->nik = $request->nik;
            $ed->nama_kk = $request->nama_kk;
            $ed->jml_kelg = $request->jml_kelg;
            $ed->lat = $request->lat;
            $ed->lng = $request->lng; 
            $ed->save();
            return response(["status"=>"success"],200);
         
    }

   /*  public function delete(Request $request)
    {
        $bg = Wrgmap::find($request->id);
        $bg->delete();
        return response(["status"=>"success"],200);
    } */
}
