<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Service;

class Auth extends Controller
{
    public function index(Request $request)
    {
         $user = User::all()->where('username','=',$request->usr);
         if(count($user) > 0){
            $pass = $user[0]->password;
            if(password_verify($request->pwd,$pass)){
                $svc = new Service();
                $token = $svc->generateToken($user[0]->id);
                return response(['status'=>'success','token'=>$token],200);
            } else {
                return response(['status'=>'wrong password'],400);
            }
         } else {
            return response(['status'=>'unauthorized'],400);
         }
    }

    public function userValidate(Request $request)
    {
        if(!$request->user_status){
            return response(['status'=>'unauthorized'],400);
        } else {
            return response(['status'=>'success',"user_id"=>$request->user_status],200);
        }
    }
}
