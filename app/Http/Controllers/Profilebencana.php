<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Models\Geodisaster;
use App\Http\Service;

class Profilebencana extends Controller
{
    public function add(Request $request)
    {
         //cari tahun data
        
            $ed = new Geodisaster();
            $ed->tahun = $request->tahun;
            $ed->nama_dusun = $request->nama_dusun;
            $ed->radius_bencana = $request->radius_bencana;
            $ed->rumah_rusak = $request->rumah_rusak;
            $ed->korban_jiwa = $request->korban_jiwa;
            $ed->lat = $request->lat;
            $ed->lng = $request->lng; 
            $ed->save();
            return response(["status"=>"success"],200);
    }

    public function list(Request $request)
    {
         $res = Geodisaster::all(); 
         return response(["status"=>"success","data"=>$res],200);
    }

   public function search(Request $request)
    {
        $res = Geodisaster::find($request->id);
        return response(['status'=>'success','data'=>$res],200);
    }

    public function update(Request $request)
    {
    
            $ed =  Geodisaster::find($request->id);
            $ed->nik = $request->nik;
            $ed->nama_kk = $request->nama_kk;
            $ed->jml_kelg = $request->jml_kelg;
            $ed->lat = $request->lat;
            $ed->lng = $request->lng; 
            $ed->save();
            return response(["status"=>"success"],200);
         
    }

   /*  public function delete(Request $request)
    {
        $bg = Geodisaster::find($request->id);
        $bg->delete();
        return response(["status"=>"success"],200);
    } */
}
