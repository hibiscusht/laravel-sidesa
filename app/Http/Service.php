<?php

namespace App\Http;

class Service {

    private function hasExpired($ts)
    {
        $last = 6 * 60 * 60;
        $diff = strtotime(date('Y-m-d H:i:s')) - strtotime($ts);
        if($diff <= $last){
            return false;
        } else {
            return true;
        }
    }

    public function generateToken($id)
    {
        $ident = base64_encode('SIDESA');
        $created = base64_encode(date('Y-m-d H:i:s'));
        $payload = base64_encode($id);
        return $ident . '.' . $created . '.' . $payload;
    }

    public function validateToken($token)
    {
        $data = explode('.',$token);
        if(base64_decode($data[0]) != 'SIDESA'){
            return false;
        } else if($this->hasExpired(base64_decode($data[1]))){
            return false;
        } else {
            return base64_decode($data[2]);
        }
    }
}