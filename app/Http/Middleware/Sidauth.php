<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request; 
use App\Http\Service;

class Sidauth
{

    /**
     * Help disclose Auth bearer token
     * 
     * @return user detail
     */

    private function discloseAuthBearerToken()
    {
        $hdr = getallheaders();
        $auth = $hdr["authorization"];
        $token = preg_replace('/Bearer\s/','',$auth);
        return $token;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $this->discloseAuthBearerToken();
        $svc = new Service();
        $request->user_status = $svc->validateToken($token);
        return $next($request);
    }
}
