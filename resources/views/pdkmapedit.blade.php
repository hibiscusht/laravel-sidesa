@extends('layout')

@section('sebaran')
active
@endsection

@section('main')
<main class="content container-lg mx-auto pt-2">
    <center><h1>TAMBAH DATA SEBARAN PENDUDUK DESA CIKALAN</h1></center>
    <div class="card">
    <div class="card-header">
        <h4 class="card-title"></h4>
        <div></div>
    </div>
    <div class="card-body">
        <h5></h5>
        <div class="mb-4 row">
                    <label class="col-3 col-form-label required">NIK</label>
                    <div class="col">
                         <input type="text" class="form-control map" data-key="nik">
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Nama KK</label>
                    <div class="col">
                    <input type="text" class="form-control map" data-key="nama_kk">
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Jumlah Anggota Keluarga</label>
                    <div class="col">
                    <input type="number" class="form-control map" value=1 min="1" data-key="jml_kelg">
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Lokasi Rumah</label>
                    <div class="col">
                    <div id="map_canvas" style="height: 500px"><i class="fa fa-spinner fa-spin fa-fw"></i> menunggu Google Maps</div>
                    <input type="hidden" id="lat" class="map" data-key="lat"><input type="hidden" id="lng" class="map" data-key="lng">
                        <div class="form-hint"></div>
                    </div>
                </div>
                
    </div>
    <div class="card-footer">
        <button class="btn btn-default" onclick="saveMe()">Simpan</button>
        <a href="{{ route('pdk-map') }}"><button class="btn btn-danger">Batal</button></a>
    </div> 
    <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwqoMDt8aFQji_MDMqbsyV0j2VMJeMbRw&callback=loadMap"></script>
    <script>

    async function loadDetail(id)
    {
        const req = await fetch("{{ url('api/profil/sebaran/search') }}"+"?id="+id)
        const res = await req.json()
        if(req.ok){
            const elem = document.querySelectorAll('.map')
            for(let e of elem){
                e.value = res.data[e.dataset.key]
            }
            return {lat:parseFloat(res.data.lat),lng:parseFloat(res.data.lng)}
        }
           
    }

        window.loadMap = async function()
        {
            const id = "{{$id}}"
            const openpos = await loadDetail(id)
             // Creating map object
             const map = new google.maps.Map(document.getElementById('map_canvas'), {
                zoom: 12,
                center: new google.maps.LatLng(-7.5696706,110.4577053),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            // creates a draggable marker to the given coords
            const vMarker = new google.maps.Marker({
                position: openpos,
                draggable: true
            });

            // adds a listener to the marker
            // gets the coords when drag event ends
            // then updates the input with the new coords
            google.maps.event.addListener(vMarker, 'dragend', function (evt) {
                const lat = evt.latLng.lat().toFixed(6);
                const lng = evt.latLng.lng().toFixed(6);
				document.querySelector('#lat').value = lat
                document.querySelector('#lng').value = lng

			// var latlng = new google.maps.LatLng(lat, lng);
			/* var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
						$("#coded").val(results[1].formatted_address); 
                    }
                }
            }); */

                map.panTo(evt.latLng);
            });

            // centers the map on markers coords
            map.setCenter(vMarker.position);

            // adds the marker on the map
            vMarker.setMap(map);
        }
    </script>
    <script>
 

        async function saveMe()
        {
           const fd = new FormData
            const elem = document.querySelectorAll('.map')
            for(let e of elem){
                fd.append(e.dataset.key,e.value)
            } 
            const req = await fetch("{{ url('api/profil/sebaran/update') }}?id={{$id}}",{
                method: "POST",
                body: fd
            })
            const res = await req.json()
            if(req.ok){
                alert('berhasil menyimpan data')
                location.href = "{{ route('pdk-map') }}"
            } else {
                alert(res.status)
            }
        }
    </script>
</main>
@endsection