@extends('layout')

@section('disaster')
active
@endsection

@section('main')
<main class="content container-lg mx-auto pt-2">
    <center><h1>PETA WILAYAH RAWAN BENCANA DESA CIKALAN</h1></center>
    <div class="card">
    <div class="card-header">
        <h4 class="card-title"></h4>
        <div>
            <a href="{{ route('pdk-disaster-add') }}"><button class="btn btn-default">Tambah Data</button></a>
        </div>
    </div>
    <div class="card-body">
        <h5></h5>
        <div id="map_canvas" style="height: 500px"><i class="fa fa-spinner fa-spin fa-fw"></i> menunggu Google Maps</div><br/>
    </div>
    <div class="card-footer">
        
    </div>
    </div>
     <!-- Custom Stylesheet -->
     <link href="{{ asset('js/datatable/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
     <!--<link href="{{ asset('js/datatable/style.css') }}" rel="stylesheet">-->
     <script src="{{ asset('js/datatable/common.min.js') }}"></script>
    <script src="{{ asset('js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatable/dataTables.bootstrap4.min.js') }}"></script> 
    <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwqoMDt8aFQji_MDMqbsyV0j2VMJeMbRw&callback=loadMap"></script>
    <script>

    async function loadLocation()
    {
        const req = await fetch("{{ url('api/profil/bencana/list') }}")
        const res = await req.json()
        if(req.ok){
            return res.data
        }
    }

        window.loadMap = async function()
        {
             // Creating map object
             const map = new google.maps.Map(document.getElementById('map_canvas'), {
                zoom: 12,
                center: new google.maps.LatLng(-7.5696706,110.4577053),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            const loc = await loadLocation()

            loc.map((pos, i) => {
                const cont = `
                <table>
                <tr>
                <td>Nama Dusun:</td>
                <td>${pos.nama_dusun}</td>
                </tr>
                <tr>
                <td>Tahun Kejadian:</td>
                <td>${pos.tahun}</td>
                </tr>
                <tr>
                <td>Korban Jiwa:</td>
                <td>${pos.korban_jiwa}</td>
                </tr>
                <tr>
                <td>Bangunan Terdampak:</td>
                <td>${pos.rumah_rusak}</td>
                </tr>
                <tr>
                <td></td>
                <td><a href="{{ route('pdk-disaster-edit') }}?id=${pos.id}">edit</a></td>
                </tr>
                </table>
                `
                const infowindow = new google.maps.InfoWindow({
                    content: cont 
                });
                const marker = new google.maps.Marker({
                position: {lat: parseFloat(pos.lat),lng: parseFloat(pos.lng)},
                map 
                })
                 // Add the circle for this city to the map.
                const cityCircle = new google.maps.Circle({
                strokeColor: "#FF0000",
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: "#FF0000",
                fillOpacity: 0.35,
                map,
                center: {lat: parseFloat(pos.lat),lng: parseFloat(pos.lng)},
                radius: (pos.radius_bencana) * 1000,
                })
                marker.addListener("click", () => {
                infowindow.open({
                anchor: marker,
                map,
                })
                })
             })
        } 
    </script> 
</main>
@endsection