@extends('layout')

@section('sebaran')
active
@endsection

@section('main')
<main class="content container-lg mx-auto pt-2">
    <center><h1>PETA SEBARAN PENDUDUK DESA CIKALAN</h1></center>
    <div class="card">
    <div class="card-header">
        <h4 class="card-title"></h4>
        <div>
            <a href="{{ route('pdk-map-add') }}"><button class="btn btn-default">Tambah Data</button></a>
        </div>
    </div>
    <div class="card-body">
        <h5></h5>
        <div id="map_canvas" style="height: 500px"><i class="fa fa-spinner fa-spin fa-fw"></i> menunggu Google Maps</div><br/>
    </div>
    <div class="card-footer">
        
    </div>
    </div>
     <!-- Custom Stylesheet -->
     <link href="{{ asset('js/datatable/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
     <!--<link href="{{ asset('js/datatable/style.css') }}" rel="stylesheet">-->
     <script src="{{ asset('js/datatable/common.min.js') }}"></script>
    <script src="{{ asset('js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatable/dataTables.bootstrap4.min.js') }}"></script> 
    <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwqoMDt8aFQji_MDMqbsyV0j2VMJeMbRw&callback=loadMap"></script>
    <script>

    async function loadLocation()
    {
        const req = await fetch("{{ url('api/profil/sebaran/list') }}")
        const res = await req.json()
        if(req.ok){
            return res.data
        }
    }

        window.loadMap = async function()
        {
             // Creating map object
             const map = new google.maps.Map(document.getElementById('map_canvas'), {
                zoom: 12,
                center: new google.maps.LatLng(-7.5696706,110.4577053),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            const loc = await loadLocation()

            loc.map((pos, i) => {
                const cont = `
                <table>
                <tr>
                <td>Nama KK:</td>
                <td>${pos.nama_kk}</td>
                </tr>
                <tr>
                <td>Jml Kelg:</td>
                <td>${pos.jml_kelg}</td>
                </tr>
                <tr>
                <td></td>
                <td><a href="{{ route('pdk-map-edit') }}?id=${pos.id}">edit</a></td>
                </tr>
                </table>
                `
                const infowindow = new google.maps.InfoWindow({
                    content: cont 
                });
                const marker = new google.maps.Marker({
                position: {lat: parseFloat(pos.lat),lng: parseFloat(pos.lng)},
                map 
                })
                marker.addListener("click", () => {
                infowindow.open({
                anchor: marker,
                map,
                })
                })
             })
        } 
    </script> 
</main>
@endsection