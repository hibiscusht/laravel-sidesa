<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Cikalan | SIDESA</title>
        <base href="/">
        <link rel="stylesheet" href="{{ asset('css/flatmin/main.css') }}">
        <script src="{{ asset('js/flatmin/main.js') }}"></script>
    </head>
    <body>
        
    <div class="d-flex flex-grow-1 align-items-center">
    <div class="content container max-w-md">
        <div class="content-header">
            <div class="content-title mx-auto mt-6 mb-5">
                <img src="{{ asset('img/cikalan.png') }}" alt="" class="h-12" style="margin-top: -12%; margin-right: -2%"> SIDESA Cikalan
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="card-title mt-2 mb-5">Silakan Login</h5>
                <div>
                    <div class="mb-4">
                        <label for="exampleInputEmail1" class="form-label">Username</label>
                        <input type="text" class="form-control" id="usr" aria-describedby="emailHelp">
                    </div>
                    <div class="mb-4">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input type="password" class="form-control" id="pwd">
                    </div>
                    <div class="form-check">
                       
                    </div>
                </div>
            </div>
            <div class="card-footer text-end py-3">
                <button class="btn btn-primary" onclick="validateCredential()">Login</button>
            </div>
        </div>
    </div>
</div>
    <div class="text-center">
        <footer>
    <div class="container">
        <span class="text-secondary text-sm text-secondary">
            <span>Made with </span>
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-heart w-5" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                <path d="M19.5 12.572l-7.5 7.428l-7.5 -7.428m0 0a5 5 0 1 1 7.5 -6.566a5 5 0 1 1 7.5 6.572"></path>
            </svg>
            <span> love and </span>
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-coffee w-5" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                <path d="M3 14c.83 .642 2.077 1.017 3.5 1c1.423 .017 2.67 -.358 3.5 -1c.83 -.642 2.077 -1.017 3.5 -1c1.423 -.017 2.67 .358 3.5 1"></path>
                <path d="M8 3a2.4 2.4 0 0 0 -1 2a2.4 2.4 0 0 0 1 2"></path>
                <path d="M12 3a2.4 2.4 0 0 0 -1 2a2.4 2.4 0 0 0 1 2"></path>
                <path d="M3 10h14v5a6 6 0 0 1 -6 6h-2a6 6 0 0 1 -6 -6v-5z"></path>
                <path d="M16.746 16.726a3 3 0 1 0 .252 -5.555"></path>
            </svg>
            <span> caffeine </span>
        </span>
    </div>
</footer>
    </div>

<script>
    const session = localStorage.getItem('sidesa-session')
    if(session == undefined){} else {
        location.href = "{{ route('dash') }}"
    }

    async function validateCredential()
    {
        const usr = document.querySelector('#usr').value
        const pwd = document.querySelector('#pwd').value
        const fd = new FormData
        fd.append('usr',usr)
        fd.append('pwd',pwd)
        const hdr = new Headers
       // hdr.append('Authorization','Bearer abc123XZy')
        const req = await fetch("{{ url('api/login') }}",{
            method: "POST",
            body: fd,
            headers: hdr
        })
        const res = await req.json()
        if(req.ok){
            localStorage.setItem('sidesa-session',res.token)
            location.href = "{{ route('dash') }}"
        } else {
            alert(res.status)
        }
    }
</script>

    </body>
</html>