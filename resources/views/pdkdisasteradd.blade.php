@extends('layout')

@section('disaster')
active
@endsection

@section('main')
<main class="content container-lg mx-auto pt-2">
    <center><h1>TAMBAH DATA WILAYAH RAWAN BENCANA DESA CIKALAN</h1></center>
    <div class="card">
    <div class="card-header">
        <h4 class="card-title"></h4>
        <div></div>
    </div>
    <div class="card-body">
        <h5></h5>
        <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Tahun Kejadian</label>
                    <div class="col">
                         <input type="number" class="form-control map" value="2000" data-key="tahun" min="2000">
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Nama Dusun</label>
                    <div class="col">
                    <input type="text" class="form-control map" data-key="nama_dusun">
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Radius Terdampak (km)</label>
                    <div class="col">
                    <input type="number" class="form-control map" value=1 min="1" data-key="radius_bencana">
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Korban (jiwa)</label>
                    <div class="col">
                    <input type="number" class="form-control map" value=0 min="0" data-key="korban_jiwa">
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Bangunan Terdampak (unit)</label>
                    <div class="col">
                    <input type="number" class="form-control map" value=0 min="0" data-key="rumah_rusak">
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Lokasi Kejadian</label>
                    <div class="col">
                    <div id="map_canvas" style="height: 500px"><i class="fa fa-spinner fa-spin fa-fw"></i> menunggu Google Maps</div>
                    <input type="hidden" id="lat" class="map" data-key="lat"><input type="hidden" id="lng" class="map" data-key="lng">
                        <div class="form-hint"></div>
                    </div>
                </div>
                
    </div>
    <div class="card-footer">
        <button class="btn btn-default" onclick="saveMe()">Simpan</button>
        <a href="{{ route('pdk-disaster') }}"><button class="btn btn-danger">Batal</button></a>
    </div> 
    <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwqoMDt8aFQji_MDMqbsyV0j2VMJeMbRw&callback=loadMap"></script>
    <script>
        window.loadMap = function()
        {
             // Creating map object
             var map = new google.maps.Map(document.getElementById('map_canvas'), {
                zoom: 12,
                center: new google.maps.LatLng(-7.5696706,110.4577053),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            // creates a draggable marker to the given coords
            var vMarker = new google.maps.Marker({
                position: new google.maps.LatLng(-7.5696706,110.4577053),
                draggable: true
            });

            // adds a listener to the marker
            // gets the coords when drag event ends
            // then updates the input with the new coords
            google.maps.event.addListener(vMarker, 'dragend', function (evt) {
                var lat = evt.latLng.lat().toFixed(6);
                var lng = evt.latLng.lng().toFixed(6);
				document.querySelector('#lat').value = lat
                document.querySelector('#lng').value = lng

			// var latlng = new google.maps.LatLng(lat, lng);
			/* var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
						$("#coded").val(results[1].formatted_address); 
                    }
                }
            }); */

                map.panTo(evt.latLng);
            });

            // centers the map on markers coords
            map.setCenter(vMarker.position);

            // adds the marker on the map
            vMarker.setMap(map);
        }
    </script>
    <script>
 

        async function saveMe()
        {
           const fd = new FormData
            const elem = document.querySelectorAll('.map')
            for(let e of elem){
                fd.append(e.dataset.key,e.value)
            } 
            const req = await fetch("{{ url('api/profil/bencana/add') }}",{
                method: "POST",
                body: fd
            })
            const res = await req.json()
            if(req.ok){
                alert('berhasil menyimpan data')
                location.href = "{{ route('pdk-disaster') }}"
            } else {
                alert(res.status)
            }
        }
    </script>
</main>
@endsection