@extends('layout')

@section('latar')
active
@endsection

@section('main')
<main class="content container-lg mx-auto pt-2">
    <center><h1>LATAR DESA CIKALAN</h1></center>
    <div class="card">
    <div class="card-header">
        <h4 class="card-title"></h4>
        <div>
            <a href="{{ route('pro-latar-add') }}"><button class="btn btn-default">Tambah Data</button></a>
        </div>
    </div>
    <div class="card-body">
        <h5></h5>
        <table class="server-side table table-striped table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kategori</th>
                    <th>Nama File</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <div class="card-footer">
        
    </div>
    </div>
     <!-- Custom Stylesheet -->
     <link href="{{ asset('js/datatable/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
     <!--<link href="{{ asset('js/datatable/style.css') }}" rel="stylesheet">-->
     <script src="{{ asset('js/datatable/common.min.js') }}"></script>
    <script src="{{ asset('js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatable/dataTables.bootstrap4.min.js') }}"></script> 
    <script>
        let dt = ""
        async function loadData()
        {
            dt = $('.server-side').DataTable({
                "language": {
                  "emptyTable": "Data kosong",
                  "zeroRecords": "Data tidak ditemukan"
              }, 
              "ajax": {
                        "url": "{{ url('api/profil/latar/list') }}",
                        "type": "POST"
                      }, 
              "processing": true,
              "serverSide": true,
              "columns": [
                {"data":"no"},
                {"data":"type"},
                {"data":"filename"},
                {"data":(row)=>{
                    return `<a href="{{ route('pro-latar-edit') }}?id=${row.id}"><button class="btn btn-info" >Edit</button></a>&nbsp;<button class="btn btn-danger" onclick="clearMe(${row.id})">Delete</button>`
                }}
              ]
            })
        }
        loadData()
        async function clearMe(id)
        {
            const ask = confirm('apakah yakin akan menghapus?')
            if(ask){
                const req = await fetch("{{ url('api/profil/latar/delete') }}?id=" + id)
                if(req.ok){
                    alert('berhasil menghapus')
                    dt.ajax.reload()
                }
            }
        }
    </script>
</main>
@endsection