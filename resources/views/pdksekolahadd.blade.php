@extends('layout')

@section('sekolah')
active
@endsection

@section('main')
<main class="content container-lg mx-auto pt-2">
    <center><h1>TAMBAH DATA JUMLAH PENDUDUK DESA CIKALAN MENURUT PENDIDIKAN</h1></center>
    <div class="card">
    <div class="card-header">
        <h4 class="card-title"></h4>
        <div></div>
    </div>
    <div class="card-body">
        <h5></h5>
        <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Tahun</label>
                    <div class="col">
                         <input type="number" class="form-control sekolah" value=0 min="2000" max="2030" data-key="tahun">
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Jumlah Tidak Sekolah</label>
                    <div class="col">
                    <input type="number" class="form-control sekolah" value=0 data-key="tdk_sekolah">
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Jumlah Prasekolah</label>
                    <div class="col">
                    <input type="number" class="form-control sekolah" value=0 data-key="prasekolah">
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Jumlah SD</label>
                    <div class="col">
                    <input type="number" class="form-control sekolah" value=0 data-key="sd">
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Jumlah SMP</label>
                    <div class="col">
                    <input type="number" class="form-control sekolah" value=0 data-key="smp">
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Jumlah SMA</label>
                    <div class="col">
                    <input type="number" class="form-control sekolah" value=0 data-key="sma">
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Jumlah S1</label>
                    <div class="col">
                    <input type="number" class="form-control sekolah" value=0 data-key="s1">
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Jumlah S2</label>
                    <div class="col">
                    <input type="number" class="form-control sekolah" value=0 data-key="s2">
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Jumlah S3</label>
                    <div class="col">
                    <input type="number" class="form-control sekolah" value=0 data-key="s3">
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Jumlah D3</label>
                    <div class="col">
                    <input type="number" class="form-control sekolah" value=0 data-key="d3">
                        <div class="form-hint"></div>
                    </div>
                </div>
    </div>
    <div class="card-footer">
        <button class="btn btn-default" onclick="saveMe()">Simpan</button>
        <a href="{{ route('pdk-sekolah') }}"><button class="btn btn-danger">Batal</button></a>
    </div> 
    <script>
 

        async function saveMe()
        {
           const fd = new FormData
            const elem = document.querySelectorAll('.sekolah')
            for(let e of elem){
                fd.append(e.dataset.key,e.value)
            } 
            const req = await fetch("{{ url('api/profil/sekolah/add') }}",{
                method: "POST",
                body: fd
            })
            const res = await req.json()
            if(req.ok){
                alert('berhasil menyimpan data')
                location.href = "{{ route('pdk-sekolah') }}"
            } else {
                alert(res.status)
            }
        }
    </script>
</main>
@endsection