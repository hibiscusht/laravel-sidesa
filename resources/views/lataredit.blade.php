@extends('layout')

@section('latar')
active
@endsection

@section('main')
<main class="content container-lg mx-auto pt-2">
    <center><h1>EDIT LATAR DESA CIKALAN</h1></center>
    <div class="card">
    <div class="card-header">
        <h4 class="card-title"></h4>
        <div></div>
    </div>
    <div class="card-body">
        <h5></h5>
        <div class="mb-4 row">
                    <label class="col-3 col-form-label required">Kategori Latar Belakang</label>
                    <div class="col">
                         <select class="form-control latar" name="" id="" data-key="type">
                            <option value="logo">Logo</option>
                            <option value="banner-atas">Banner Atas</option>
                            <option value="gambar-tengah">Gambar Tengah</option>
                            <option value="gambar-kiri-1">Gambar Kiri 1</option>
                            <option value="gambar-kiri-2">Gambar Kiri 2</option>
                            <option value="gambar-kanan">Gambar Kanan</option>
                            <option value="banner-bawah">Banner Bawah</option>
                         </select>
                        <div class="form-hint"></div>
                    </div>
                </div>
                <div class="mb-4 row">
                    <label class="col-3 col-form-label required">File</label>
                    <div class="col">
                    <input type="file" id="file">
                        <div class="form-hint"></div>
                    </div>
                </div>
    </div>
    <div class="card-footer">
        <button class="btn btn-default" onclick="saveMe()">Simpan</button>
        <a href="{{ route('pro-latar') }}"><button class="btn btn-danger">Batal</button></a>
    </div>
    
    <script>

        async function loadData()
        {
            const req = await fetch("{{ url('api/profil/latar/search') }}?id={{ $id }}")
            const res = await req.json()
            if(req.ok){
                const elem = document.querySelectorAll('.latar')
                for(let e of elem){ 
                  e.value = res.data[e.dataset.key]
                } 
            }
        }

        loadData()


        async function saveMe()
        {
            const file = document.querySelector('#file')
            const fd = new FormData
            fd.append("file",file.files[0])
            const elem = document.querySelectorAll('.latar')
            for(let e of elem){
                fd.append(e.dataset.key,e.value)
            } 
            const req = await fetch("{{ url('api/profil/latar/update') }}?id={{ $id }}",{
                method: "POST",
                body: fd
            })
            const res = await req.json()
            if(req.ok){
                alert('berhasil mengupdate')
                location.href = "{{ route('pro-latar') }}"
            } else {
                alert(res.status)
            }
        }
    </script>
</main>
@endsection