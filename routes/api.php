<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route; 
use App\Http\Controllers\Auth;
use App\Http\Controllers\Profilelatar;
use App\Http\Controllers\Profilesekolah;
use App\Http\Controllers\Profilemap;
use App\Http\Controllers\Profilebencana;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
 
Route::post('/user/validate',[Auth::class,"userValidate"])->middleware('sidauth');

Route::post('/login',[Auth::class,"index"]);

Route::post('/profil/latar/upload',[Profilelatar::class,"upload"]);

Route::post('/profil/latar/list',[Profilelatar::class,"list"]);

Route::get('/profil/latar/search',[Profilelatar::class,"search"]);

Route::post('/profil/latar/update',[Profilelatar::class,"update"]);

Route::get('/profil/latar/delete',[Profilelatar::class,"delete"]);

Route::post('/profil/sekolah/add',[Profilesekolah::class,"add"]);

Route::post('/profil/sekolah/list',[Profilesekolah::class,"list"]);

Route::post('/profil/sebaran/add',[Profilemap::class,"add"]);

Route::get('/profil/sebaran/list',[Profilemap::class,"list"]);

Route::get('/profil/sebaran/search',[Profilemap::class,"search"]);

Route::post('/profil/sebaran/update',[Profilemap::class,"update"]);

Route::get('/profil/bencana/list',[Profilebencana::class,"list"]);

Route::post('/profil/bencana/add',[Profilebencana::class,"add"]);

