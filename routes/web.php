<?php

use Illuminate\Support\Facades\Route; 
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});*/ 

Route::get('/', function () {
    return view('landing');
}); 

Route::get('/login', function () {
    return view('login');
}); 

Route::get('/dashboard', function () {
    return view('dashboard');
})->name('dash'); 

Route::get('/profil/latar', function () {
    return view('latar');
})->name('pro-latar');

Route::get('/profil/latar/add', function () {
    return view('lataradd');
})->name('pro-latar-add'); 

Route::get('/profil/latar/edit', function (Request $request) {
    return view('lataredit',['id'=>$request->id]);
})->name('pro-latar-edit'); 

Route::get('/profil/info', function () {
    return view('info');
})->name('pro-info'); 

Route::get('/penduduk/sekolah', function () {
    return view('pdksekolah');
})->name('pdk-sekolah'); 

Route::get('/penduduk/sekolah/add', function () {
    return view('pdksekolahadd');
})->name('pdk-sekolah-add'); 

Route::get('/penduduk/sekolah/edit', function () {
    return view('pdksekolahedit');
})->name('pdk-sekolah-edit'); 

Route::get('/penduduk/agama', function () {
    return view('pdkagama');
})->name('pdk-agama'); 

Route::get('/penduduk/sebaran', function () {
    return view('pdkmap');
})->name('pdk-map'); 

Route::get('/penduduk/sebaran/add', function () {
    return view('pdkmapadd');
})->name('pdk-map-add');

Route::get('/penduduk/sebaran/edit', function (Request $request) {
    return view('pdkmapedit',["id"=>$request->id]);
})->name('pdk-map-edit');

Route::get('/geografis/bencana', function () {
    return view('pdkdisaster');
})->name('pdk-disaster'); 

Route::get('/geografis/bencana/add', function () {
    return view('pdkdisasteradd');
})->name('pdk-disaster-add');

Route::get('/geografis/bencana/edit', function () {
    return view('pdkdisasteredit');
})->name('pdk-disaster-edit'); 
